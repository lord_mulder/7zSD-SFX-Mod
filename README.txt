7-Zip SFX module for installers - modified version
==================================================

Introduction
------------

This is a *modified* version of the 7-Zip SFX module for installers. Modifications made by MuldeR <mulder2@gmx.de>.

All modifications in this version are contributed to the public domain!


Modifications
-------------

Modifications in this version include:

1. Added a "requestedExecutionLevel" manifest to the executable, so the SFX wrapper can run in "User" or "Administrator" context, as needed.
   Pick "asInvoker" version to run in User context, or pick "requireAdministrator" version to run in Administrator context!
   (With the original 7-Zip SFX binary, Windows decides on its own whether it should run as "User" or as "Administrator", which is insecure)

2. Added a "compatibility" manifest to the executable, which prevents modern Windows from suggesting to restart the installer in compatibility mode.
   This happens under certain circumstances, when the required compatibility manifest is missing - and it can be really annoying!

3. Added workaround to prevent error messages and UAC dialogs from disappearing in the background. We need to give it a proper Window handle!
   For this purpose, a popup window (banner) will be shown, with with "topmost" style, while the embedded installer is starting up.
   Also, all the error messages will be displayed with "topmost" style now as well, so that the user is guaranteed to actually see them.

4. If setup could not be launched, e.g. because user has canceled the UAC dialog, show proper message box and give user chance to retry.
   The original 7-Zip SFX code will simply fail and exit immediately in that case - which is not the best idea, IMHO.

5. Implemented a fallback method for locating the "Temp" directory, used when the %TMP% environment variable does *not* point to a valid directory.
   The original 7-Zip SFX code assumes that %TMP% points to a valid directory with "write" access. But this certainly can *not* be relied on!
   (Try setting the %TMP% environment variable to an invalid directory, or a directory without write access, and see how original 7-Zip SFX fails)

6. Implemented a fallback method for removing the extracted temporary files from "Temp" directory when the SFX module terminates.
   The original 7-Zip SFX code only tries once, but this will fail occasionally! The new code will retry, if temporary files could not be deleted.

7. Implemented a more robust method to read the "attached" configuration from the SFX file when then SFX module is starting up.
   The original 7-Zip SFX code only tries once, but this will fail occasionally! The new code will retry, if file could not be opened immediately.


Additional SFX Parameters
-------------------------

The following parameters can be used in the "config.txt" used to build your SFX installer:

"LaunchingMsg":
    Status message that will be displayed in the popup window (banner) while embedded installer is starting up

"ExecuteErrorMsg":
    Error message that will be shown if embedded installer failed to launch, e.g. UAC dialog declined by user


Acknowledgment
--------------

7-Zip for installers is part of LZMA SDK.

LZMA SDK is written and placed in the public domain by Igor Pavlov.
